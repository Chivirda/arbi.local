<div id="container">
	<div class="v-page-heading v-fancy-heading v-bg-stylish light-style v-fancy-image v-fancy-top-header" style="background-image: url(include/photos/_a-slider-1.jpg); background-size: cover;">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="heading-text">
						<h1 class="entry-title text-aling-left">Аренда офисов</h1>
						<h2 class="entry-subtitle no-margin text-aling-left">Предоставление офисных помещений на льготной основе</h2>
					</div>
					<ol class="breadcrumb">
						<li><a href="#" class="white-color">Главная</a></li>
						<li class="active white-color">Услуги</li>
						<li class="active white-color">Аренда офисов</li>
					</ol>
				</div>
			</div>
		</div>
	</div>
	<div class="v-page-wrap no-bottom-spacing">
		<div class="container">
			<div class="v-spacer col-sm-12 v-height-mini">
			</div>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-xm-12">
					<div class="v-heading-v3">
						<h2>Комплексное решение для Вашего бизнеса</h2>
						<div class="horizontal-break">
						</div>
					</div>
				</div>
				<div class="col-sm-6">
					<p>
						 Предметом деятельности ГАУ АО «Архангельский региональный бизнес-инкубатор» является помощь предпринимателям на начальной стадии их деятельности, позволяющая сэкономить средства за счет невысоких арендных платежей и использования дополнительных услуг бизнес-инкубатора. Бизнес-инкубатором осуществляется предоставление офисных помещений на льготной основе для начинающих субъектов малого предпринимательства, оборудованных мебелью, оргтехникой, компьютерами с лицензионным программным обеспечением, а также имеются точки доступа к Интернету и городскому телефону.
					</p>
				</div>
				<div class="col-sm-6">
					<p>
						 Главная задача Архангельского регионального бизнес-инкубатора — помочь тем, кто оtткрывает собственное дело, особенно на начальной стадии деятельности, а также оказывать поддержку существующим малым и средним предприятиям Архангельской области.
					</p>
					<p>
						 Это достигается путем предоставления этим фирмам материальных, информационных, консультационных и других необходимых услуг для развития малых и средних предприятий на территории нашей области
					</p>
				</div>
			</div>
		</div>
	</div>
	 <?$APPLICATION->IncludeComponent(
	"bitrix:news.detail",
	"uslugi_office",
	Array(
		"ADD_ELEMENT_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "Y",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"BROWSER_TITLE" => "-",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_ACTIVE_DATE_FORMAT" => "d.m.Y",
		"DETAIL_DISPLAY_BOTTOM_PAGER" => "Y",
		"DETAIL_DISPLAY_TOP_PAGER" => "N",
		"DETAIL_FIELD_CODE" => array("",""),
		"DETAIL_PAGER_SHOW_ALL" => "Y",
		"DETAIL_PAGER_TEMPLATE" => "",
		"DETAIL_PAGER_TITLE" => "Страница",
		"DETAIL_PROPERTY_CODE" => array("",""),
		"DETAIL_SET_CANONICAL_URL" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "3",
		"IBLOCK_TYPE" => "info",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
		"LIST_ACTIVE_DATE_FORMAT" => "d.m.Y",
		"LIST_FIELD_CODE" => array("",""),
		"LIST_PROPERTY_CODE" => array("",""),
		"MESSAGE_404" => "",
		"META_DESCRIPTION" => "-",
		"META_KEYWORDS" => "-",
		"NEWS_COUNT" => "20",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PREVIEW_TRUNCATE_LEN" => "",
		"SEF_MODE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "Y",
		"SHOW_404" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"USE_CATEGORIES" => "N",
		"USE_FILTER" => "N",
		"USE_PERMISSIONS" => "N",
		"USE_RATING" => "N",
		"USE_RSS" => "N",
		"USE_SEARCH" => "N",
		"USE_SHARE" => "N",
		"VARIABLE_ALIASES" => Array("ELEMENT_ID"=>"ELEMENT_ID","SECTION_ID"=>"SECTION_ID")
	)
);?><br>
</div>
 <br>