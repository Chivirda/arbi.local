	<div class="v-page-wrap no-bottom-spacing no-top-spacing">
		<div class="v-bg-stylish v-bg-stylish-v4">
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<div class="v-heading-v3">
							<h2><span>Специализация деятельности</span></h2>
							<div class="horizontal-break"></div>
						</div>
						<p>
							Предметом деятельности ГАУ АО «Архангельский региональный бизнес-инкубатор»  является помощь предпринимателям на начальной стадии их деятельности, позволяющая сэкономить средства за счет невысоких арендных платежей и использования дополнительных услуг бизнес-инкубатора. Бизнес-инкубатором осуществляется предоставление офисных помещений на льготной основе для начинающих субъектов малого предпринимательства, оборудованных мебелью, оргтехникой, компьютерами с лицензионным программным обеспечением, а также имеются точки доступа к Интернету и городскому телефону.
						</p>
						<p>
							Кроме того, ГАУ АО «Архангельский региональный бизнес-инкубатор» осуществляет:
						</p>
						<ul class="_a7">
							<li class="_a7__item">организация консультационных услуг по вопросам бухгалтерского учета и налогообложения, коммерческой деятельности, составление бизнес-планов и т.д;</li>
							<li class="_a7__item">сопровождение работы информационного сайта www.msp29.ru портал малого и среднего предпринимательства Архангельская область;</li>
							<li class="_a7__item">проведение конференций, тренингов, семинаров, лекций, круглых столов и т.д на актуальные темы для субъектов малого и среднего предпринимательства.</li>
						</ul>
					</div>

					<div class="col-sm-6">
						<div class="v-heading-v3">
							<h2><span>Руководство инкубатора</span></h2>
							<div class="horizontal-break"></div>
						</div>
						<div class="row">
							<div class="col-sm-6">
								<div class="v-team-member-box">
									<div class="cover">
										<div class="v-team-member-img">
												<img src="<?=SITE_TEMPLATE_PATH?>/img/team/_a-team-1.jpg" alt="Olivia Marica">
										</div>
										<div class="member-info">
											<div class="heading">
												<div class="v-team-member-info">
													<h4 class="v-team-member-name">Деев Денис <br> Валериевич</h4>
													<span class="v-team-member-statu">исполняющий обязанности директора</span>
												</div>
											</div>
											<div class="v-team-member-contacts">
												<span>Телефон: (8182) 42-14-53</span>
												<span class="hide-text">Email:</span>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="v-team-member-box">
									<div class="cover">
										<div class="v-team-member-img">
												<img src="<?=SITE_TEMPLATE_PATH?>/img/team/_a-team-2.jpg" alt="Olivia Marica">
										</div>
										<div class="member-info">
											<div class="heading">
												<div class="v-team-member-info">
													<h4 class="v-team-member-name">Павлова Ирина <br> Николаевна</h4>
													<span class="v-team-member-statu">руководитель центра поддержки предпринимательства</span>
												</div>
											</div>
											<div class="v-team-member-contacts">
												<span>Телефон: (8182) 42-14-53</span>
												<span>pavlova@msp29.ru</span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
