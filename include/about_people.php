<div class="v-bg-stylish v-bg-stylish-v4">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="v-heading-v3">
					<h2><span>Наша команда</span></h2>
					<div class="horizontal-break"></div>
				</div>
			</div>
			<div class="v-team-member-wrap">
				<div class="v-team-member-box col-sm-3">
					<div class="cover">
						<div class="v-team-member-img">
								<img src="<?=SITE_TEMPLATE_PATH?>/img/team/_a-team-1.jpg" alt="Olivia Marica">
						</div>
						<div class="member-info">
							<div class="heading">
								<div class="v-team-member-info">
									<h4 class="v-team-member-name">Деев Денис <br> Валериевич</h4>
									<span class="v-team-member-statu">исполняющий обязанности директора</span>
								</div>
							</div>
							<div class="v-team-member-contacts">
								<span>Телефон: (8182) 42-14-53</span>
								<span>pavlova@msp29.ru</span>
							</div>
						</div>
					</div>
				</div>
				<div class="v-team-member-box col-sm-3">
					<div class="cover">
						<div class="v-team-member-img">
								<img src="<?=SITE_TEMPLATE_PATH?>/img/team/_a-team-2.jpg" alt="Olivia Marica">
						</div>
						<div class="member-info">
							<div class="heading">
								<div class="v-team-member-info">
									<h4 class="v-team-member-name">Павлова Ирина <br> Николаевна</h4>
									<span class="v-team-member-statu">руководитель центра поддержки предпринимательства</span>
								</div>
							</div>
							<div class="v-team-member-contacts">
								<span>Телефон: (8182) 42-14-53</span>
								<span>pavlova@msp29.ru</span>
							</div>
						</div>
					</div>
				</div>
				<div class="v-team-member-box col-sm-3">
					<div class="cover">
						<div class="v-team-member-img">
							<img src="<?=SITE_TEMPLATE_PATH?>/img/team/_a-team-3.jpg" alt="Dan Petrovsky">
						</div>
						<div class="member-info">
							<div class="heading">
								<div class="v-team-member-info">
									<h4 class="v-team-member-name">Самойлов Константин</h4>
									<span class="v-team-member-statu">исполняющий обязанности директора</span>
								</div>
							</div>
							<div class="v-team-member-contacts">
								<span>Телефон: (8182) 42-14-53</span>
								<span>bigsun65@example.com</span>
							</div>
						</div>
					</div>
				</div>
				<div class="v-team-member-box  col-sm-3">
					<div class="cover">
						<div class="v-team-member-img">
							<img src="<?=SITE_TEMPLATE_PATH?>/img/team/_a-team-4.jpg" alt="Mike Delphino">
						</div>
						<div class="member-info">
							<div class="heading">
								<div class="v-team-member-info">
									<h4 class="v-team-member-name">Токарев Валентин Георгиевич </h4>
									<span class="v-team-member-statu">Staff manager</span>
								</div>
							</div>

							<div class="v-team-member-contacts">
								<span>Телефон: (8182) 42-14-53</span>
								<span>gooddeath39@example.com</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>