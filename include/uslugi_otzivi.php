<div class="v-bg-stylish padding-60">
		<div class="container">
				<div class="row center">
					<div class="col-sm-12">
						<h2 class="_a-h2">Отзывы арендаторов</h2>
					</div>
				</div>

				<div class="row padding-30">
						<div class="col-sm-4">
								<div class="testimonial testimonial-thumb-side v-animation" data-animation="fade-from-bottom" data-delay="0">
										<div class="testimonial-author">
												<figure class="featured-thumbnail">
														<img src="include/photos/team08.jpg">
												</figure>
										</div>

										<div class="wrapper">
												<div class="excerpt">
														Proin fermentum, augue id porttitor condimentum, tellus nisi dapibus est, ultricies
														malesuada metus massa a orci. Donec consequat ornare erat, vitae vehicula orci feugiat ac.
												</div>
												<div class="user">- Королёв Степан, <span>CEO</span></div>
										</div>
								</div>
						</div>

						<div class="col-sm-4">
								<div class="testimonial testimonial-thumb-side v-animation" data-animation="fade-from-bottom" data-delay="200">
										<div class="testimonial-author">
												<figure class="featured-thumbnail">
														<img src="include/photos/team07.jpg">
												</figure>
										</div>

										<div class="wrapper">
												<div class="excerpt">
														Lorem ipsum dolor sit amet elit, consectetur. Suspendisse viverra
														mauris eget tortor imperdiet vehicula. Proin egestas diam ac mauris molestie hendrerit nec nisi tortor.
												</div>
												<div class="user">- Сорокина Екатерина, <span>CEO</span></div>
										</div>
								</div>
						</div>

						<div class="col-sm-4">
								<div class="testimonial testimonial-thumb-side v-animation" data-animation="fade-from-bottom" data-delay="400">
										<div class="testimonial-author">
												<figure class="featured-thumbnail">
														<img src="include/photos/team06.jpg">
												</figure>
										</div>

										<div class="wrapper">
												<div class="excerpt">
														Proin fermentum, augue id porttitor condimentum, tellus nisi dapibus est, ultricies
														malesuada metus massa a orci. Donec consequat ornare erat, vitae vehicula orci feugiat ac.
												</div>
												<div class="user">- Борисов Валерий,
													<span>Web developer </span></div>
										</div>
								</div>
						</div>
				</div>
		</div>
</div>
