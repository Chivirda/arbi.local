<div class="v-page-wrap no-bottom-spacing no-top-spacing">
	<div class="fw-map-wrapper">

		<div class="fw-map">
			<div class="v-gmap-widget fullscreen-map">

				<!-- Full Width -->
				<div id="googlemapsFullWidth" style="height:900px" class="google-map mt-none mb-none"></div>
			</div>
		</div>

		<div class="container">

			<div class="map-info-section top-to-bottom">

				<div class="v-heading-v2">
					<h4>Контактная информация</h4>
				</div>

				<div class="row">
					<div class="col-sm-12">
						<ul class="v-list">
							<li>
								<i class="fa fa-map-marker"></i>
								<span>163060, г. Архангельск, </span>
							</li>
							<li>
								<i class="fa fa-building"></i>
								<span>пр. Обводный канал, д. 12, оф. 501, 505</span>
							</li>
							<li>
								<i class="fa fa-phone"></i>
								<span>(8182) 42-14-53, 42-14-63</span>
							</li>
							<li>
								<i class="fa fa-envelope-o"></i>
								<span>office@msp29.ru</span>
							</li>
						</ul>
					</div>
				</div>

				<div class="v-heading-v2">
					<h4>Напишите нам</h4>
				</div>

				<form class="special-contact-form">

					<div class="input-group">
						<input type="text" class="form-control" name="special-contact-name" placeholder="Ваше имя" />
						<input type="email" class="form-control" name="special-contact-email" placeholder="Email" />

						<textarea class="form-control" rows="3" name="message" placeholder="Сообщение"></textarea>
					</div>

					<a href="#" class="btn v-btn v-third-dark">Отправить</a>
				</form>
			</div>
		</div>
	</div>
</div>

