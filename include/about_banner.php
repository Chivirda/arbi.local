<div class="v-page-heading v-fancy-heading v-bg-stylish light-style v-fancy-image v-fancy-top-header" style="background-image: url(<?=SITE_TEMPLATE_PATH?>/img/slider/_a-slider-2.jpg); background-size: cover;">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="heading-text">
					<h1 class="entry-title text-aling-left">О бизнес-инкубаторе</h1>
					<h2 class="entry-subtitle no-margin text-aling-left">Помощь предпринимателям на начальной стадии их деятельности</h2>
				</div>
				<ol class="breadcrumb">
					<li><a href="#" class="white-color">Главная</a></li>
					<li class="active white-color">О бизнес-инкубаторе</li>
				</ol>
			</div>
		</div>
	</div>
</div>