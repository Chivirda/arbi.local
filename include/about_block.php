			<div class="container">

				<div class="row">

					<div class="col-sm-6">
						<div class="feature-box left-icon-v2 v-animation" data-animation="flip" data-delay="200">
							<i class="fa fa-building v-icon icn-holder medium"></i>
							<div class="feature-box-text">
								<h3>Аренда офисов и конференц-зала</h3>
								<div class="feature-box-text-inner">
									<p>
										Предоставление офисных помещений на льготной основе для начинающих субъектов малого предпринимательства, оборудованных мебелью, оргтехникой, компьютерами с лицензионным программным обеспечением
									</p>
								</div>
							</div>
						</div>

						<p>&nbsp;</p>

						<div class="feature-box left-icon-v2 v-animation" data-animation="flip" data-delay="400">
							<i class="fa fa-book v-icon icn-holder medium"></i>
							<div class="feature-box-text">
								<h3>Консалтинг и обучающие мероприятия</h3>
								<div class="feature-box-text-inner">
									<p>
										Организация консультационных услуг по вопросам бухгалтерского учета и налогообложения, коммерческой деятельности, составление бизнес-планов и т.д;
									</p>
								</div>
							</div>
						</div>

						<p>&nbsp;</p>

						<div class="feature-box left-icon-v2 v-animation" data-animation="flip" data-delay="600">
							<i class="fa fa-trophy v-icon icn-holder medium"></i>
							<div class="feature-box-text">
								<h3>Конкурсы и субсидии</h3>
								<div class="feature-box-text-inner">
									<p>
										Проведение конференций, тренингов, семинаров, лекций, круглых столов и т.д на актуальные темы для субъектов малого и среднего предпринимательства.
									</p>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
