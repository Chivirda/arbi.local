		<div class="footer-wrap">
			<footer>
				<div class="container">
					<div class="row">
						<div class="col-sm-3">
							<section class="widget">
								<img alt="Volvox" src="<?= SITE_TEMPLATE_PATH; ?>/img/_a-logo-2.png" style="height: 65px; margin-bottom: 20px;">
								<? $APPLICATION->IncludeFile("include/footer-text.php", array(), array("MODE"=>"html")); ?>
							</section>
						</div>
						<div class="col-sm-3">
							<section class="widget v-recent-entry-widget">
								<div class="widget-heading">
									<h4>Основные Разделы сайта</h4>
									<div class="horizontal-break"></div>
								</div>
								<?$APPLICATION->IncludeComponent("bitrix:menu", "down", Array(
									"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
										"CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
										"DELAY" => "N",	// Откладывать выполнение шаблона меню
										"MAX_LEVEL" => "1",	// Уровень вложенности меню
										"MENU_CACHE_GET_VARS" => array(	// Значимые переменные запроса
											0 => "",
										),
										"MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
										"MENU_CACHE_TYPE" => "N",	// Тип кеширования
										"MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
										"ROOT_MENU_TYPE" => "top",	// Тип меню для первого уровня
										"USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
									),
									false
								);?>
							</section>
						</div>
						<div class="col-sm-3">
							<section class="widget v-recent-entry-widget">
								<div class="widget-heading">
									<h4>последние новости</h4>
									<div class="horizontal-break"></div>
								</div>
                                <?$APPLICATION->IncludeComponent("bitrix:news.list", "last-news", Array(
                                    "ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
                                        "ADD_SECTIONS_CHAIN" => "Y",	// Включать раздел в цепочку навигации
                                        "AJAX_MODE" => "N",	// Включить режим AJAX
                                        "AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
                                        "AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
                                        "AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
                                        "AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
                                        "CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
                                        "CACHE_GROUPS" => "Y",	// Учитывать права доступа
                                        "CACHE_TIME" => "36000000",	// Время кеширования (сек.)
                                        "CACHE_TYPE" => "A",	// Тип кеширования
                                        "CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
                                        "DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
                                        "DISPLAY_BOTTOM_PAGER" => "N",	// Выводить под списком
                                        "DISPLAY_DATE" => "Y",	// Выводить дату элемента
                                        "DISPLAY_NAME" => "Y",	// Выводить название элемента
                                        "DISPLAY_PICTURE" => "Y",	// Выводить изображение для анонса
                                        "DISPLAY_PREVIEW_TEXT" => "Y",	// Выводить текст анонса
                                        "DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
                                        "FIELD_CODE" => array(	// Поля
                                            0 => "",
                                            1 => "",
                                        ),
                                        "FILTER_NAME" => "",	// Фильтр
                                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
                                        "IBLOCK_ID" => "2",	// Код информационного блока
                                        "IBLOCK_TYPE" => "info",	// Тип информационного блока (используется только для проверки)
                                        "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",	// Включать инфоблок в цепочку навигации
                                        "INCLUDE_SUBSECTIONS" => "Y",	// Показывать элементы подразделов раздела
                                        "MESSAGE_404" => "",	// Сообщение для показа (по умолчанию из компонента)
                                        "NEWS_COUNT" => "4",	// Количество новостей на странице
                                        "PAGER_BASE_LINK_ENABLE" => "N",	// Включить обработку ссылок
                                        "PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
                                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
                                        "PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
                                        "PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
                                        "PAGER_TEMPLATE" => ".default",	// Шаблон постраничной навигации
                                        "PAGER_TITLE" => "Новости",	// Название категорий
                                        "PARENT_SECTION" => "",	// ID раздела
                                        "PARENT_SECTION_CODE" => "",	// Код раздела
                                        "PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
                                        "PROPERTY_CODE" => array(	// Свойства
                                            0 => "",
                                            1 => "",
                                        ),
                                        "SET_BROWSER_TITLE" => "Y",	// Устанавливать заголовок окна браузера
                                        "SET_LAST_MODIFIED" => "N",	// Устанавливать в заголовках ответа время модификации страницы
                                        "SET_META_DESCRIPTION" => "Y",	// Устанавливать описание страницы
                                        "SET_META_KEYWORDS" => "Y",	// Устанавливать ключевые слова страницы
                                        "SET_STATUS_404" => "N",	// Устанавливать статус 404
                                        "SET_TITLE" => "Y",	// Устанавливать заголовок страницы
                                        "SHOW_404" => "N",	// Показ специальной страницы
                                        "SORT_BY1" => "ACTIVE_FROM",	// Поле для первой сортировки новостей
                                        "SORT_BY2" => "SORT",	// Поле для второй сортировки новостей
                                        "SORT_ORDER1" => "DESC",	// Направление для первой сортировки новостей
                                        "SORT_ORDER2" => "ASC",	// Направление для второй сортировки новостей
                                    ),
                                    false
                                );?>
							</section>
						</div>
						<div class="col-sm-3">
							<section class="widget">
								<div class="widget-heading">
									<h4>Галерея</h4>
									<div class="horizontal-break"></div>
								</div>
								<ul class="portfolio-grid">
									<li>
										<a href="portfolio-single.html" class="grid-img-wrap">
											<img src="<?= SITE_TEMPLATE_PATH; ?>/img/thumbs/_a-project-1.jpg" />
											<span class="tooltip">Phasellus enim libero<span class="arrow"></span></span>
										</a>
									</li>
									<li>
										<a href="portfolio-single.html" class="grid-img-wrap">
											<img src="<?= SITE_TEMPLATE_PATH; ?>/img/thumbs/_a-project-2.jpg" />
											<span class="tooltip">Phasellus enim libero<span class="arrow"></span></span>
										</a>
									</li>
									<li>
										<a href="portfolio-single.html" class="grid-img-wrap">
											<img src="<?= SITE_TEMPLATE_PATH; ?>/img/thumbs/_a-project-3.jpg" />
											<span class="tooltip">Phasellus enim<span class="arrow"></span></span>
										</a>
									</li>
									<li>
										<a href="portfolio-single.html" class="grid-img-wrap">
											<img src="<?= SITE_TEMPLATE_PATH; ?>/img/thumbs/_a-project-4.jpg" />
											<span class="tooltip">Phasellus Enim<span class="arrow"></span></span>
										</a>
									</li>
									<li>
										<a href="portfolio-single.html" class="grid-img-wrap">
											<img src="<?= SITE_TEMPLATE_PATH; ?>/img/thumbs/_a-project-5.jpg" />
											<span class="tooltip">Phasellus Enim libero<span class="arrow"></span></span>
										</a>
									</li>
									<li>
										<a href="portfolio-single.html" class="grid-img-wrap">
											<img src="<?= SITE_TEMPLATE_PATH; ?>/img/thumbs/_a-project-6.jpg" />
											<span class="tooltip">Phasellus Enim<span class="arrow"></span></span>
										</a>
									</li>
								</ul>
							</section>
						</div>
					</div>
				</div>
			</footer>

        <? $APPLICATION->IncludeFile("include/footer-copy.php", array(), array("MODE"=>"html")); ?>
		</div>
		<!--End Footer-Wrap-->
	</div>

	<!--// BACK TO TOP //-->
	<div id="back-to-top" class="animate-top"><i class="fa fa-angle-up"></i></div>

	<!-- Libs -->
	<script src="<?= SITE_TEMPLATE_PATH;?>/js/jquery.min.js"></script>
	<script src="<?= SITE_TEMPLATE_PATH;?>/js/bootstrap.min.js"></script>
	<script src="<?= SITE_TEMPLATE_PATH;?>/js/jquery.flexslider-min.js"></script>
	<script src="<?= SITE_TEMPLATE_PATH;?>/js/jquery.easing.js"></script>
	<script src="<?= SITE_TEMPLATE_PATH;?>/js/jquery.fitvids.js"></script>
	<script src="<?= SITE_TEMPLATE_PATH;?>/js/jquery.carouFredSel.min.js"></script>
	<script src="<?= SITE_TEMPLATE_PATH;?>/js/jquery.validate.js"></script>
	<script src="<?= SITE_TEMPLATE_PATH;?>/js/theme-plugins.js"></script>
	<script src="<?= SITE_TEMPLATE_PATH;?>/js/jquery.isotope.min.js"></script>
	<script src="<?= SITE_TEMPLATE_PATH;?>/js/imagesloaded.js"></script>
	<script src="<?= SITE_TEMPLATE_PATH;?>/js/view.min.js?auto"></script>

	<script src="<?= SITE_TEMPLATE_PATH;?>/plugins/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
	<script src="<?= SITE_TEMPLATE_PATH;?>/plugins/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

	<script src="<?= SITE_TEMPLATE_PATH;?>/js/theme-core.js"></script>
	
	<script src="<?= SITE_TEMPLATE_PATH;?>/js/bootstrap-timepicker.js"></script>
	<script src="<?= SITE_TEMPLATE_PATH;?>/js/bootstrap-datepicker.js"></script>
	<script src="<?= SITE_TEMPLATE_PATH;?>/js/jquery.maskedinput.js"></script>
	<script src="<?= SITE_TEMPLATE_PATH;?>/js/theme-form-element.js"></script>

<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
?>
	</body>
</html>