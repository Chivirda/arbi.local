<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
?>
<!DOCTYPE html>
<html>
	<head>
		<?$APPLICATION->ShowHead();?>
		<title><?$APPLICATION->ShowTitle();?></title>
		<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />
			<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<link rel="shortcut icon" type="image/png" href="<?= SITE_TEMPLATE_PATH;?>/img/favicon.png" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- Web Fonts  -->
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,500,600,700,800" rel="stylesheet" type="text/css">


		<!-- Libs CSS -->
		<link href="<?= SITE_TEMPLATE_PATH;?>/css/bc-style.css" rel="stylesheet" />
		<link href="<?= SITE_TEMPLATE_PATH;?>/css/search.css" rel="stylesheet" />
		<link href="<?= SITE_TEMPLATE_PATH;?>/css/bootstrap.min.css" rel="stylesheet" />
		<link href="<?= SITE_TEMPLATE_PATH;?>/css/style.css" rel="stylesheet" />
		<link href="<?= SITE_TEMPLATE_PATH;?>/css/font-awesome.min.css" rel="stylesheet" />
		<link href="<?= SITE_TEMPLATE_PATH;?>/css/streamline-icon.css" rel="stylesheet" />
		<link href="<?= SITE_TEMPLATE_PATH;?>/css/v-nav-menu.css" rel="stylesheet" />
		<link href="<?= SITE_TEMPLATE_PATH;?>/css/v-portfolio.css" rel="stylesheet" />
		<link href="<?= SITE_TEMPLATE_PATH;?>/css/v-blog.css" rel="stylesheet" />
		<link href="<?= SITE_TEMPLATE_PATH;?>/css/v-animation.css" rel="stylesheet" />
		<link href="<?= SITE_TEMPLATE_PATH;?>/css/v-bg-stylish.css" rel="stylesheet" />
		<link href="<?= SITE_TEMPLATE_PATH;?>/css/v-shortcodes.css" rel="stylesheet" />
		<link href="<?= SITE_TEMPLATE_PATH;?>/css/theme-responsive.css" rel="stylesheet" />
		<link href="<?= SITE_TEMPLATE_PATH;?>/plugins/owl-carousel/owl.theme.css" rel="stylesheet" />
		<link href="<?= SITE_TEMPLATE_PATH;?>/plugins/owl-carousel/owl.carousel.css" rel="stylesheet" />

		<!-- Current Page CSS -->
		<link href="<?= SITE_TEMPLATE_PATH;?>/plugins/rs-plugin/css/settings.css" rel="stylesheet" />
		<link href="<?= SITE_TEMPLATE_PATH;?>/plugins/rs-plugin/css/custom-captions.css" rel="stylesheet" />

		<!-- Custom CSS -->
		<link rel="stylesheet" href="<?= SITE_TEMPLATE_PATH;?>/css/custom.css">
	</head>
	<body>
		<div id="panel">
			<?$APPLICATION->ShowPanel();?>
		</div>

		<?$APPLICATION->IncludeFile("include/top-line.php", array(), array("MODE"=>"html"));?>

	<div class="header-container">
		<header class="header fixed clearfix">

			<div class="container">

				<!--Site Logo-->
				<div class="logo">
					<a href="/">
						<img alt="АРБИ" src="<?= SITE_TEMPLATE_PATH;?>/img/_a-logo.png" data-logo-height="35">
					</a>
				</div>
				<!--End Site Logo-->

				<div class="navbar-collapse nav-main-collapse collapse">

					<!--Header Search-->
					<div class="search" id="headerSearch">
						<a href="#" id="headerSearchOpen"><i class="fa fa-search"></i></a>
						<div class="search-input">

							<?$APPLICATION->IncludeComponent(
                                "bitrix:search.form",
                                "search-form",
                                array(
                                    "PAGE" => "#SITE_DIR#search/index.php",
                                    "USE_SUGGEST" => "N",
                                    "COMPONENT_TEMPLATE" => "search-form"
                                ),
                                false
                            );?>
							<span class="v-arrow-wrap"><span class="v-arrow-inner"></span></span>
						</div>
					</div>
					<!--End Header Search-->
					<!--Main Menu-->
					<nav class="nav-main mega-menu">
						<?$APPLICATION->IncludeComponent(
	"bitrix:menu",
	"top",
	array(
		"ALLOW_MULTI_SELECT" => "N",
		"CHILD_MENU_TYPE" => "left",
		"DELAY" => "N",
		"MAX_LEVEL" => "1",
		"MENU_CACHE_GET_VARS" => array(
		),
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_TYPE" => "N",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"ROOT_MENU_TYPE" => "top",
		"USE_EXT" => "N",
		"COMPONENT_TEMPLATE" => "top"
	),
	false
);?>
					</nav>
					<!--End Main Menu-->
				</div>
				<button class="btn btn-responsive-nav btn-inverse" data-toggle="collapse" data-target=".nav-main-collapse">
					<i class="fa fa-bars"></i>
				</button>
			</div>

			<span class="v-header-shadow"></span>
		</header>

	</div>