<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>
<ul class="nav nav-pills nav-main" id="mainMenu">

<?
foreach($arResult as $arItem):
	if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) 
		continue;
?>
	<?if($arItem["SELECTED"]):?>
		<li><a href="<?=$arItem["LINK"]?>" class="selected"><?=$arItem["TEXT"]?></a></li>
		<?elseif($arItem["TEXT"] == "Услуги"): ?>
		<li class="dropdown"><a class="dropdown-toggle" href="#"><?=$arItem["TEXT"]?><i class="fa fa-caret-down"></i></a>
			<ul class="dropdown-menu">
				<li><a class="current" href="index.html">Home - Variation 1</a></li>
				<li><a href="index-2.html">Home - Variation 2</a></li>
				<li><a href="index-3.html">Home - Variation 3</a></li>
				<li><a href="index-4.html">Home - Variation 4</a></li>
				<li><a href="index-5.html">Home - Variation 5 <span class="v-menu-item-info bg-warning">Boxed</span></a></li>
				<li><a href="index-6.html">Home - Variation 6</a></li>
				<li><a href="index-7.html">Home - Variation 7</a></li>
				<li><a href="index-8.html">Home - Variation 8</a></li>
				<li><a href="index-9.html">Home - Variation 9 <span class="v-menu-item-info bg-info">Flat</span></a></li>
				<li><a href="index-10.html">Home - Variation 10</a></li>
				<li><a href="index-11.html">Home - Variation 11</a></li>
				<li><a href="index-12.html">Home - Variation 12</a></li>
				<li><a href="index-13.html">Home - Variation 13</a></li>
				<li><a href="index-14.html">Home - Variation 14</a></li>
				<li><a href="page-landing.html">Home - Variation 15 <span class="v-menu-item-info bg-success">Landing</span></a></li>
			</ul>
		</li>
	<?else:?>
		<li><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>
	<?endif?>
	
<?endforeach?>

</ul>
<?endif?>