<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?><br />
<?endif;?>
<?foreach($arResult["ITEMS"] as $arItem):?>
	<div class="_a-events__item">
		<img class="_a-events__img" src="<?= $arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="events img 1">
		<a class="_a-events__text" href="<?echo $arItem["DETAIL_PAGE_URL"]?>">
			<?echo $arItem["NAME"]?>
		</a>
		<span class="_a-events__date"><?echo $arItem["DISPLAY_ACTIVE_FROM"];?></span>
	</div>
<?endforeach;?>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>
