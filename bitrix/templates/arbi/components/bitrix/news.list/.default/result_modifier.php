<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

foreach($arResult["ITEMS"] as &$arItem) {
	$section = CIBlockSection::GetByID($arItem['IBLOCK_SECTION_ID'])->Fetch();
	$arItem['SECTION_NAME'] =  $section['NAME'];
	$arItem['SECTION_CODE'] =  $section['CODE'];
	list($arItem['DAY'],$arItem['MONTH'],$arItem['YEAR'])=explode(" ", $arItem['DISPLAY_ACTIVE_FROM']);
}
?>