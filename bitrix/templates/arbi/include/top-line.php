<div class="_a-top-line">
	<div class="container">
		<div class="row">
			<div class="_a-top-line_col col-sm-3">
				<span class="_a-top-line__item">
					<i class="fa fa-mobile-phone" aria-hidden="true"></i>
					<span class="_a-top-line__text">
						8 (8182) 42-14-53
					</span>
					<span class="_a-top-line__text _a-top-line__text_sm">
						(будни с 10:00 до 18:00)
					</span>
				</span>
			</div>
			<div class="_a-top-line_col col-sm-4">
				<span class="_a-top-line__item">
					<i class="fa fa-envelope" aria-hidden="true"></i>
					<a class="_a-top-line__text" href="#">
						office@msp29.ru
					</a>
				</span>
			</div>
			<div class="_a-top-line_col col-sm-2 col-md-offset-3">
				<span class="_a-top-line__item _a-top-line__item_right">
					<i class="fa fa-user" aria-hidden="true"></i>
					<a class="_a-top-line__text" href="#">
						Личный кабинет
					</a>
				</span>
			</div>
		</div>

	</div>
</div>