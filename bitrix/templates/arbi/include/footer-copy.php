<div class="copyright">
    <div class="container">
        <p>&copy; 2016. Архангельский Бизнес Инкубатор</p>

        <ul class="social-icons std-menu pull-right">
            <li><a href="http://www.twitter.com/" target="_blank" data-placement="top" rel="tooltip" title="" data-original-title="Twitter"><i class="fa fa-twitter"></i></a></li>
            <li><a href="http://www.facebook.com/" target="_blank" data-placement="top" rel="tooltip" title="" data-original-title="Facebook"><i class="fa fa-facebook"></i></a></li>
            <li><a href="http://www.google.com/" target="_blank" data-placement="top" rel="tooltip" title="" data-original-title="Google+"><i class="fa fa-google-plus"></i></a></li>
            <li><a href="http://www.linkedin.com/" target="_blank" data-placement="top" rel="tooltip" title="" data-original-title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
            <li><a href="http://www.youtube.com/" target="_blank" data-placement="top" rel="tooltip" title="" data-original-title="Linkedin"><i class="fa fa-youtube"></i></a></li>
        </ul>

    </div>
</div>