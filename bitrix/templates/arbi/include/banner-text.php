<div class="container _a-banner__container">
	<h2 class="_a-banner__heading">
		Архангельский Региональный <br> Бизнес Инкубатор
	</h2>
	<ul class="_a-banner-list">
		<li class="_a-banner-list__item">Аренда офисов</li>
		<li class="_a-banner-list__item">Конференц-зал</li>
		<li class="_a-banner-list__item">Консалтинг и обучающие мероприятия</li>
		<li class="_a-banner-list__item">Конкурсы и субсидии </li>
		<li class="_a-banner-list__item">Иные услуги</li>
	</ul>
	<a class="_a-banner-button" href="o-biznes-inkubatore/">
		<i class="fa fa-arrow-right _a-banner-button__icon"></i>
		<span class="_a-banner-button__text">узнать подробнее</span>
	</a>
</div>