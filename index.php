<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php');
$APPLICATION->SetTitle('Главная');
?> 
	<div id="container">
		<div class="_a-banner" style="background-image: url(<?= SITE_TEMPLATE_PATH;?>/img/banners/_a-banner.jpg)">
			<? $APPLICATION->IncludeFile("include/banner-text.php", array(), array("MODE"=>"html")); ?>
		</div>
		<div class="no-bottom-spacing">

			<div class="container _a-m_1">
				<div class="row">
					<div class="col-sm-6">
						<div class="_a-news">
							<div class="_a-news__header">
								<div class="_a-news__title">
									Новости
								</div>
								<a class="_a-news__action" href="#">
									<i class="fa fa-newspaper-o"></i>Подписаться на новости
								</a>
							</div>
							<?$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"main-template-news", 
	array(
		"ACTIVE_DATE_FORMAT" => "j F Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "novosti-i-sobytiya/#SECTION_CODE#/#ELEMENT_CODE#",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(
			0 => "DATE_ACTIVE_FROM",
			1 => "",
		),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "2",
		"IBLOCK_TYPE" => "info",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "6",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "6",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"COMPONENT_TEMPLATE" => "main-template-news"
	),
	false
);?>	
						</div>
					</div>
					<div class="col-sm-6">
						<div class="_a-events">
							<div class="_a-events__header">
								<div class="_a-events__title">
									События
								</div>
							</div>

							<?$APPLICATION->IncludeComponent("bitrix:news.list", "main-template-events", Array(
	"ACTIVE_DATE_FORMAT" => "j F Y",	// Формат показа даты
		"ADD_SECTIONS_CHAIN" => "Y",	// Включать раздел в цепочку навигации
		"AJAX_MODE" => "N",	// Включить режим AJAX
		"AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
		"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
		"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
		"AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
		"CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
		"CACHE_GROUPS" => "Y",	// Учитывать права доступа
		"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
		"CACHE_TYPE" => "A",	// Тип кеширования
		"CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
		"DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
		"DISPLAY_BOTTOM_PAGER" => "N",	// Выводить под списком
		"DISPLAY_DATE" => "Y",	// Выводить дату элемента
		"DISPLAY_NAME" => "Y",	// Выводить название элемента
		"DISPLAY_PICTURE" => "Y",	// Выводить изображение для анонса
		"DISPLAY_PREVIEW_TEXT" => "Y",	// Выводить текст анонса
		"DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
		"FIELD_CODE" => array(	// Поля
			0 => "",
			1 => "",
		),
		"FILTER_NAME" => "",	// Фильтр
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
		"IBLOCK_ID" => "2",	// Код информационного блока
		"IBLOCK_TYPE" => "info",	// Тип информационного блока (используется только для проверки)
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",	// Включать инфоблок в цепочку навигации
		"INCLUDE_SUBSECTIONS" => "N",	// Показывать элементы подразделов раздела
		"MESSAGE_404" => "",	// Сообщение для показа (по умолчанию из компонента)
		"NEWS_COUNT" => "20",	// Количество новостей на странице
		"PAGER_BASE_LINK_ENABLE" => "N",	// Включить обработку ссылок
		"PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
		"PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
		"PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
		"PAGER_TEMPLATE" => ".default",	// Шаблон постраничной навигации
		"PAGER_TITLE" => "Новости",	// Название категорий
		"PARENT_SECTION" => "5",	// ID раздела
		"PARENT_SECTION_CODE" => "",	// Код раздела
		"PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
		"PROPERTY_CODE" => array(	// Свойства
			0 => "",
			1 => "",
		),
		"SET_BROWSER_TITLE" => "Y",	// Устанавливать заголовок окна браузера
		"SET_LAST_MODIFIED" => "N",	// Устанавливать в заголовках ответа время модификации страницы
		"SET_META_DESCRIPTION" => "Y",	// Устанавливать описание страницы
		"SET_META_KEYWORDS" => "Y",	// Устанавливать ключевые слова страницы
		"SET_STATUS_404" => "N",	// Устанавливать статус 404
		"SET_TITLE" => "Y",	// Устанавливать заголовок страницы
		"SHOW_404" => "N",	// Показ специальной страницы
		"SORT_BY1" => "ACTIVE_FROM",	// Поле для первой сортировки новостей
		"SORT_BY2" => "SORT",	// Поле для второй сортировки новостей
		"SORT_ORDER1" => "DESC",	// Направление для первой сортировки новостей
		"SORT_ORDER2" => "ASC",	// Направление для второй сортировки новостей
	),
	false
);?>
						</div>
					</div>
				</div>
			</div>

			<div class="container">

				<div class="row">
					<div class="col-sm-12">
						<h2 class="_a-h2">
							Аренда помещний и конференц зала
						</h2>
						<div class="v-portfolio-wrap">
							<ul class="v-portfolio-items v-portfolio-standard filterable-items col-3 row clearfix">

								<li class="clearfix v-portfolio-item col-sm-4 standard   business">
									<figure class="animated-overlay overlay-alt">
										<img src="<?= SITE_TEMPLATE_PATH;?>/img/static/_a-bus-1.jpg" />
										<a href="portfolio-single.html" class="link-to-post"></a>
										<figcaption>
											<div class="thumb-info thumb-info-v2"><i class="fa fa-angle-right"></i></div>
										</figcaption>
									</figure>
									<div class="v-portfolio-item-info">
										<h3 class="v-portfolio-item-title">
											<a href="portfolio-single.html" class="link-to-post">Офис 68 м2</a>
										</h3>
										<h5 class="v-portfolio-subtitle">Обводный кан., 12</h5>
									</div>
								</li>

								<li class="clearfix v-portfolio-item col-sm-4 standard   lifestyle creation">
									<figure class="animated-overlay overlay-alt">
										<img src="<?= SITE_TEMPLATE_PATH;?>/img/static/_a-bus-2.jpg" />
										<a href="portfolio-single.html" class="link-to-post"></a>
										<figcaption>
											<div class="thumb-info thumb-info-v2"><i class="fa fa-angle-right"></i></div>
										</figcaption>
									</figure>
									<div class="v-portfolio-item-info">
										<h3 class="v-portfolio-item-title">
											<a href="portfolio-single.html" class="link-to-post">Офис 36 м2</a>
										</h3>
										<h5 class="v-portfolio-subtitle">Обводный кан., 12</h5>
									</div>
								</li>

								<li class="clearfix v-portfolio-item col-sm-4 standard   creation business">
									<figure class="animated-overlay overlay-alt">
										<img src="<?= SITE_TEMPLATE_PATH;?>/img/static/_a-bus-3.jpg" />
										<a href="portfolio-single.html" class="link-to-post"></a>
										<figcaption>
											<div class="thumb-info thumb-info-v2"><i class="fa fa-angle-right"></i></div>
										</figcaption>
									</figure>
									<div class="v-portfolio-item-info">
										<h3 class="v-portfolio-item-title">
											<a href="portfolio-single.html" class="link-to-post">Конференц зал</a>
										</h3>
										<h5 class="v-portfolio-subtitle">Обводный кан., 12</h5>
									</div>
								</li>
							</ul>
						</div>
					</div>

				</div>
			</div>
		</div>

		<div class="v-page-wrap no-bottom-spacing no-top-spacing _a-m_2">
			<h2 class="_a-h2">
				Как нас найти
			</h2>
			<? $APPLICATION->IncludeFile("include/map.php", array(), array("MODE"=>"thml")) ?>
			</div>
<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php');
?>