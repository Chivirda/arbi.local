<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Новости и события");
?><div id="container">
	<div class="v-page-heading v-bg-stylish v-bg-stylish-v1">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="heading-text">
						<h1>Подписка на рассылку</h1>
					</div>
					<ol class="breadcrumb">
						<li><a href="/">Главная</a></li>
						<li class="active">Подписка на рассылку</li>
					</ol>
				</div>
			</div>
		</div>
	</div>
	<div class="v-page-wrap has-right-sidebar has-one-sidebar">
		<div class="container">
			<div class="row">
				<div class="col-sm-9 v-blog-wrap">
					<div class="v-blog-items-wrap blog-standard">
						 <?$APPLICATION->IncludeComponent("bitrix:subscribe.edit", "template1", Array(
	"AJAX_MODE" => "N",	// Включить режим AJAX
		"AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
		"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
		"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
		"AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
		"ALLOW_ANONYMOUS" => "Y",	// Разрешить анонимную подписку
		"CACHE_TIME" => "3600",	// Время кеширования (сек.)
		"CACHE_TYPE" => "A",	// Тип кеширования
		"SET_TITLE" => "N",	// Устанавливать заголовок страницы
		"SHOW_AUTH_LINKS" => "N",	// Показывать ссылки на авторизацию при анонимной подписке
		"SHOW_HIDDEN" => "N",	// Показать скрытые рубрики подписки
	),
	false
);?><br>
 <br>
					</div>
				</div>
 <aside class="sidebar right-sidebar col-sm-3"> <section class="widget v-search-widget clearfix">
				<?$APPLICATION->IncludeComponent(
	"bitrix:search.form",
	"search-form",
	Array(
		"COMPONENT_TEMPLATE" => "search-form",
		"PAGE" => "#SITE_DIR#search/index.php",
		"USE_SUGGEST" => "N"
	)
);?> </section> <section class="widget v-category-widget clearfix">
				<div class="widget-heading clearfix">
					<h4 class="v-heading">Разделы</h4>
				</div>
				<ul>
					 <?$APPLICATION->IncludeComponent(
	"bitrix:catalog.section.list",
	"template1",
	Array(
		"ADD_SECTIONS_CHAIN" => "Y",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"COMPONENT_TEMPLATE" => "template1",
		"COUNT_ELEMENTS" => "Y",
		"IBLOCK_ID" => "2",
		"IBLOCK_TYPE" => "info",
		"SECTION_CODE" => "",
		"SECTION_FIELDS" => array(0=>"",1=>"",),
		"SECTION_ID" => $_REQUEST["SECTION_ID"],
		"SECTION_URL" => "/materialy/#SECTION_CODE_PATH#/",
		"SECTION_USER_FIELDS" => array(0=>"",1=>"",),
		"SHOW_PARENT_NAME" => "Y",
		"TOP_DEPTH" => "2",
		"VIEW_MODE" => "LINE"
	)
);?>
				</ul>
 </section> <section class="widget v-tag-cloud-widget clearfix">
				<div class="widget-heading clearfix">
					<h4 class="v-heading">Тэги</h4>
				</div>
				<div class="tagcloud">
					<ul class="wp-tag-cloud">
						 <?$APPLICATION->IncludeComponent(
	"bitrix:search.tags.cloud",
	"template1",
	Array(
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"COLOR_NEW" => "#f7f7f7",
		"COLOR_OLD" => "#f7f7f7",
		"COLOR_TYPE" => "Y",
		"COMPONENT_TEMPLATE" => "template1",
		"FILTER_NAME" => "",
		"FONT_MAX" => "11",
		"FONT_MIN" => "11",
		"PAGE_ELEMENTS" => "10",
		"PERIOD" => "10",
		"PERIOD_NEW_TAGS" => "",
		"SHOW_CHAIN" => "N",
		"SORT" => "CNT",
		"TAGS_INHERIT" => "Y",
		"URL_SEARCH" => "/search/index.php",
		"WIDTH" => "100%",
		"arrFILTER" => array(0=>"iblock_info",),
		"arrFILTER_iblock_info" => array(0=>"2",)
	)
);?>
					</ul>
				</div>
 </section> <section class="widget _a3 clearfix">
				<div class="widget-heading clearfix">
					<h4 class="v-heading">Подписка на рассылку</h4>
				</div>
				<div class="_a3__text">
					 Подпишитесь на рассылку и получайте новости на свою почту
				</div>
 <input type="text" class="form-control _a3__input" placeholder="Ваш e-mail"> <a type="button" class="btn v-btn v-third-dark _a3__button" href="#">Подписаться</a> </section> <section class="widget _a4 clearfix">
				<div class="widget-heading clearfix">
					<h4 class="v-heading">Контактная информация</h4>
				</div>
				<ul class="v-list">
					<li> <i class="fa fa-map-marker"></i>
					163060, г. Архангельск, </li>
					<li> <i class="fa fa-building"></i> <span>пр. Обводный канал, д. 12, <br>
					 оф. 501, 505</span> </li>
					<li> <i class="fa fa-phone"></i>
					(8182) 42-14-53, 42-14-63 </li>
					<li><i class="fa fa-envelope-o"></i><a style="display: inline;" href="mailto:office@msp29.ru">office@msp29.ru</a> </li>
				</ul>
 </section></aside>
			</div>
		</div>
	</div>
 <br>
</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>