<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Новости и события");
?><div id="container">
	<div class="v-page-heading v-bg-stylish v-bg-stylish-v1">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="heading-text">
						<h1>Новости и события</h1>
					</div>
					<ol class="breadcrumb">
						<li><a href="/">Главная</a></li>
						<li class="active">Новости и события</li>
					</ol>
				</div>
			</div>
		</div>
	</div>
	<div class="v-page-wrap has-right-sidebar has-one-sidebar">
		<div class="container">
			<div class="row">
				<div class="col-sm-9 v-blog-wrap">
					<div class="v-blog-items-wrap blog-standard">
						<ul class="v-blog-items row mini-items clearfix">
							 <?$APPLICATION->IncludeComponent(
	"bitrix:news",
	"news",
	Array(
		"ADD_ELEMENT_CHAIN" => "Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"BROWSER_TITLE" => "NAME",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"COMPONENT_TEMPLATE" => "news",
		"DETAIL_ACTIVE_DATE_FORMAT" => "j F Y",
		"DETAIL_DISPLAY_BOTTOM_PAGER" => "Y",
		"DETAIL_DISPLAY_TOP_PAGER" => "N",
		"DETAIL_FIELD_CODE" => array(0=>"SHOW_COUNTER",1=>"",),
		"DETAIL_PAGER_SHOW_ALL" => "Y",
		"DETAIL_PAGER_TEMPLATE" => "",
		"DETAIL_PAGER_TITLE" => "Страница",
		"DETAIL_PROPERTY_CODE" => array(0=>"",1=>"",),
		"DETAIL_SET_CANONICAL_URL" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FILE_404" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "2",
		"IBLOCK_TYPE" => "info",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"LIST_ACTIVE_DATE_FORMAT" => "j M Y",
		"LIST_FIELD_CODE" => array(0=>"SHOW_COUNTER",1=>"",),
		"LIST_PROPERTY_CODE" => array(0=>"",1=>"",),
		"MESSAGE_404" => "",
		"META_DESCRIPTION" => "-",
		"META_KEYWORDS" => "-",
		"NEWS_COUNT" => "6",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => "modern",
		"PAGER_TITLE" => "Новости",
		"PREVIEW_TRUNCATE_LEN" => "",
		"SEF_FOLDER" => "/novosti-i-sobytiya/",
		"SEF_MODE" => "Y",
		"SEF_URL_TEMPLATES" => array("news"=>"","section"=>"#SECTION_CODE_PATH#/","detail"=>"#SECTION_CODE_PATH#/#ELEMENT_CODE#",),
		"SET_LAST_MODIFIED" => "N",
		"SET_STATUS_404" => "Y",
		"SET_TITLE" => "Y",
		"SHOW_404" => "Y",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"USE_CATEGORIES" => "N",
		"USE_FILTER" => "N",
		"USE_PERMISSIONS" => "N",
		"USE_RATING" => "N",
		"USE_RSS" => "N",
		"USE_SEARCH" => "N",
		"USE_SHARE" => "N"
	)
);?>
						</ul>
					</div>
				</div>
 <aside class="sidebar right-sidebar col-sm-3"> <section class="widget v-search-widget clearfix">
				<?$APPLICATION->IncludeComponent(
	"bitrix:search.form",
	"search-form",
	Array(
		"COMPONENT_TEMPLATE" => "search-form",
		"PAGE" => "#SITE_DIR#search/index.php",
		"USE_SUGGEST" => "N"
	)
);?> </section> 
<section class="widget _a6 clearfix">
							<div class="form-group">
								<div data-plugin-datepicker data-plugin-skin="primary">
								</div>
							</div>
						</section>
<section class="widget v-category-widget clearfix">
				<div class="widget-heading clearfix">
					<h4 class="v-heading">Разделы</h4>
				</div>
				<ul>
					 <?$APPLICATION->IncludeComponent(
	"bitrix:catalog.section.list",
	"template1",
	Array(
		"ADD_SECTIONS_CHAIN" => "Y",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"COMPONENT_TEMPLATE" => "template1",
		"COUNT_ELEMENTS" => "Y",
		"IBLOCK_ID" => "2",
		"IBLOCK_TYPE" => "info",
		"SECTION_CODE" => "",
		"SECTION_FIELDS" => array(0=>"",1=>"",),
		"SECTION_ID" => $_REQUEST["SECTION_ID"],
		"SECTION_URL" => "/materialy/#SECTION_CODE_PATH#/",
		"SECTION_USER_FIELDS" => array(0=>"",1=>"",),
		"SHOW_PARENT_NAME" => "Y",
		"TOP_DEPTH" => "2",
		"VIEW_MODE" => "LINE"
	)
);?>
				</ul>
 </section> <section class="widget v-tag-cloud-widget clearfix">
				<div class="widget-heading clearfix">
					<h4 class="v-heading">Тэги</h4>
				</div>
				<div class="tagcloud">
					<ul class="wp-tag-cloud">
						 <?$APPLICATION->IncludeComponent(
	"bitrix:search.tags.cloud",
	"template1",
	Array(
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"COLOR_NEW" => "#f7f7f7",
		"COLOR_OLD" => "#f7f7f7",
		"COLOR_TYPE" => "Y",
		"COMPONENT_TEMPLATE" => "template1",
		"FILTER_NAME" => "",
		"FONT_MAX" => "11",
		"FONT_MIN" => "11",
		"PAGE_ELEMENTS" => "10",
		"PERIOD" => "10",
		"PERIOD_NEW_TAGS" => "",
		"SHOW_CHAIN" => "N",
		"SORT" => "CNT",
		"TAGS_INHERIT" => "Y",
		"URL_SEARCH" => "/search/index.php",
		"WIDTH" => "100%",
		"arrFILTER" => array(0=>"iblock_info",),
		"arrFILTER_iblock_info" => array(0=>"2",)
	)
);?>
					</ul>
				</div>
 </section> <section class="widget _a3 clearfix">
				<div class="widget-heading clearfix">
					<h4 class="v-heading">Подписка на рассылку</h4>
				</div>
				<div class="_a3__text">
					 Подпишитесь на рассылку и получайте новости на свою почту
				</div>
				 <?$APPLICATION->IncludeComponent(
	"bitrix:subscribe.form",
	"template1",
	Array(
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"PAGE" => "#SITE_DIR#subscribe",
		"SHOW_HIDDEN" => "N",
		"USE_PERSONALIZATION" => "Y"
	)
);?> </section> <section class="widget _a4 clearfix">
				<div class="widget-heading clearfix">
					<h4 class="v-heading">Контактная информация</h4>
				</div>
				<ul class="v-list">
					<li> <i class="fa fa-map-marker"></i>
					163060, г. Архангельск, </li>
					<li> <i class="fa fa-building"></i> <span>пр. Обводный канал, д. 12, <br>
					 оф. 501, 505</span> </li>
					<li> <i class="fa fa-phone"></i>
					(8182) 42-14-53, 42-14-63 </li>
					<li><i class="fa fa-envelope-o"></i><a style="display: inline;" href="mailto:office@msp29.ru">office@msp29.ru</a> </li>
				</ul>
 </section></aside>
			</div>
		</div>
	</div>
 <br>
</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>